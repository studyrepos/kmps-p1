package net.scottec.kmps.p1.a2;

import java.util.ArrayList;

public class Track{
	public String title;
	public String length;
	public int rating;
	public ArrayList<String> features = new ArrayList<String>();
	public ArrayList<String> writers = new ArrayList<String>();


	public Track set(String tag, String value) {
		switch (tag){
			case "title": this.title = value; break;
			case "length": this.length = value; break;
			case "rating": this.length = value; break;
			case "feature": this.features.add(value); break;
			case "writing": this.writers.add(value); break;
		}
		return this;
	}

	@Override
	public String toString() {
	  return "\n\tTrack:\n\t\tTitle:" + title + "\n\t\tLength:" + length + "\n\t\tRating:" + rating + "\n\t\tFeatures:" + features.toString() + "\n\t\tWriters:" + writers.toString();
	}
}
