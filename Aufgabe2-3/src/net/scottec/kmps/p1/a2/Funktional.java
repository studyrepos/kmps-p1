package net.scottec.kmps.p1.a2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Funktional {

    /**
     * Parse XML-File given as bytearray to list of tokens
     * @param content       :   filecontent as bytearray
     * @param pos           :   actual position in bytearray
     * @param tokenStream   :   Stream of tokens
     * @return Tokens as List of Strings
     */
	static List<String> createTokenList(byte[] content, int pos, Stream<String> tokenStream) {

        if (pos >= content.length)
            return tokenStream.collect(Collectors.toList());

        char c = (char) content[pos];
        if (c == 9 || c == 10 || c == 13)
            return createTokenList(content, pos + 1, tokenStream);

        Object[] token;
        if ( c == '<') {
            token = getToken(content, pos + 1, 0, '>', "");
            return createTokenList(content, pos + (int) token[0] + 2, Stream.concat(tokenStream, Stream.of((String)token[1])));
        }
        else{
            token = getToken(content, pos, 0, '<', "");
            return createTokenList(content, pos + (int) token[0], Stream.concat(tokenStream, Stream.of((String)token[1])));
        }
    }

    /**
     * Parse singel token from bytearray
     * @param content   :   bytearray to parse
     * @param pos       :   startposition for parsing
     * @param length    :   length of actual token
     * @param pattern   :   delimiter pattern
     * @param token     :   actual token
     * @return
     *      Object[  0  :   length of token
     *               1  :   token ]
     */
	static Object[] getToken(byte[] content, int pos, int length, char pattern, String token){
	    if (pos >= content.length || content[pos] == pattern)
            return new Object[] {length, token};
        return getToken(content, pos+1, length+1, pattern, token + (char)content[pos]);
    }


    /**
     * Parse a tokenList to albums and tracks
     * @param tokenList :   complete tokenList
     * @param pos       :   actual position in tokenList
     * @param alben     :   stream of already parsed albums
     * @return
     */
    static List<Album> parseTokenList(List<String> tokenList, int pos, Stream<Album> alben) {

	    if(pos >= tokenList.size())
	        return alben.collect(Collectors.toList());

	    if(tokenList.get(pos).equals("album")){
	        Object[] rec = parseAlbum(tokenList, pos, 0, new Album(), "");
	        return parseTokenList(tokenList, pos + (int)rec[0], Stream.concat(alben, Stream.of((Album)rec[1])));
        }
        else
            return parseTokenList(tokenList, pos + 1, alben);
    }

    /**
     * Parse album from tockenList
     * @param tokenList :   complete tokenlist
     * @param pos       :   actual position in tokenList
     * @param length    :   number of tokens in actual album
     * @param album     :   actual album
     * @param tag       :   tag for classmembers
     * @return Object[
     *          length  :   total number of tokens for this album
     *          album   :   the album]
     */
    static Object[] parseAlbum(List<String> tokenList, int pos, int length, Album album, String tag) {

	    if (pos >= tokenList.size())
	        return new Object[] {length, album};

	    switch(tokenList.get(pos)) {
            case "/album":
                return new Object[] {length, album};

            case "track":
                Object[] track = parseTrack(tokenList, pos+1, 0, new Track(), "");
                album.tracks.add((Track)track[1]);
                return parseAlbum(tokenList, pos+(int)track[0], length+(int)track[0], album, "");

            case "title":
            case "artist":
            case "date":
                return parseAlbum(tokenList, pos+1, length+1, album, tokenList.get(pos));

            default:
                 return parseAlbum(tokenList, pos+2, length+2, album.set(tag, tokenList.get(pos)), "");
        }
    }

    /**
     * Parse track from tockenList
     * @param tokenList :   complete tokenlist
     * @param pos       :   actual position in tokenList
     * @param length    :   number of tokens in actual track
     * @param track     :   actual track
     * @param tag       :   tag for classmembers
     * @return Object[
     *          length  :   total number of tokens for this track
     *          track   :   the track]
     */
    static Object[] parseTrack(List<String> tokenList, int pos, int length, Track track, String tag){

	    if (pos >= tokenList.size())
            return new Object[] {length, track};

        switch(tokenList.get(pos)) {
            case "/track":
                return new Object[] {length, track};

            case "title":
            case "length":
            case "rating":
            case "feature":
            case "writing":
                return parseTrack(tokenList, pos+1, length+1, track, tokenList.get(pos));

            default:
                return parseTrack(tokenList, pos+2, length+2, track.set(tag, tokenList.get(pos)), "");
        }
    }


    // just a print util without loop ...
    static int print(List<Album> alben, int pos){
        if (pos >= alben.size())
            return 0;
        System.out.println(alben.get(pos));
        return print(alben, pos+1);
    }

    /**
     *
     * @param args  :   expects path to "alben.xml" as first argument
     * @throws IOException
     */
	public static void main(String[] args) throws IOException {
		System.out.println("Praktikum 1 - Aufgabe 2 - Funktional");

		byte[] file_contents = Files.readAllBytes(Paths.get(args[0]));

		List<String> tokenList = createTokenList(file_contents, 0, Stream.empty());
		System.out.println(tokenList.toString());

		List<Album> alben = parseTokenList(tokenList, 0, Stream.empty());

        print(alben, 0);
	}
}
