package net.scottec.kmps.p1.a2;

import java.util.ArrayList;

public class Album {
	public String title;
	public String date;
	public String artist;
	public ArrayList<Track> tracks = new ArrayList<>();

	public Album set(String tag, String value) {
		switch(tag){
			case "title":
				this.title = value;
				break;
			case "artist":
				this.artist = value;
				break;
			case "date":
				this.date = value;
				break;
		}
        return this;
    }

	@Override
	public String toString() {
	  return "Album:\n\tTitle:" + title + "\n\tDate:" + date + "\n\tArtist:" + artist + "\n\tTracks:" + tracks.toString();
	}
}
